﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class BuildAssetBundles : MonoBehaviour
{
    public class Export
    {
        [MenuItem("Assets/Build AssetBundles for Android")]
        static void BuildAssetBundlesAndroid()
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/android", BuildAssetBundleOptions.None, BuildTarget.Android);
        }

        [MenuItem("Assets/Build AssetBundles for iOS")]
        static void BuildAssetBundlesiOS()
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/ios", BuildAssetBundleOptions.None, BuildTarget.iOS);
        }

        [MenuItem("Assets/Build AssetBundles for UWP")]
        static void BuildAssetBundlesUWP()
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/uwp", BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.WSAPlayer);
        }

        [MenuItem("Assets/Build AssetBundles for Windows Standalone")]
        static void BuildAssetBundlesWindowsStandalone()
        {
            BuildPipeline.BuildAssetBundles("Assets/AssetBundles/windows", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
        }

    }
}
