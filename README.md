# README #

### Building Asset Bundles ###

* Create a Unity Prefab out of the imported 3D model (Polarizer 1 included as a sample)
* For the prefab, assign a unique AssetBundle Name in the Inspector (in sample Polarizer 1, "apolar1" is the AssetBundle name)
* Click the right option under dropdown Assets -> Build AssetBundles for <PLATFORM>
* Check that the AssetBundles have been built in Assets/AssetBundles/<PLATFORM>

### Loading Asset Bundles ###

* Assign the full path to the asset bundle file (platform-specific) in the scene "Loader" GameObject script.
* Assign the prefab name to the field (platform-specific) in the scene "Loader" GameObject script. This is the name of the prefab GameObject the asset bundle is built from (for sample the prefab name is "Polarizer 1")
* Click Play, Loader will instantiate the asset bundle as a gameobject

### Additional Notes ###

* Tested in Unity 2019.4 LTS
* You need to build the appropriate asset bundle for the platform that the Unity Editor is targeting. AssetBundles are not cross platform
* The current Polarizer 1 asset bundle instantiates at the correct rotation that Ian is looking for (oriented toward camera)