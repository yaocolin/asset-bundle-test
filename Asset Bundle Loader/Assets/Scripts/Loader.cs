﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour
{
    public string FullPathAssetBundle;
    public string PrefabName;

    public void Start()
    {
        AssetBundle assetBundle = null;
        byte[] bytes = System.IO.File.ReadAllBytes(FullPathAssetBundle);
        if (bytes != null)
        {
            assetBundle = AssetBundle.LoadFromMemory(bytes);
        }
        else
        {
            Debug.LogError("Could not load bytes");
        }

        if (assetBundle != null)
        {
            Instantiate(assetBundle.LoadAsset(PrefabName) as GameObject);
        }
    }
}
